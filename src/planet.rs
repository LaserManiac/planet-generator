use std::collections::VecDeque;

use crossbeam::channel::{self, Receiver, Sender};
use glm::DVec3;
use slotmap::SlotMap;

pub use chunk::*;
pub use face::*;

use crate::graphics::terrain::TerrainVertex;

mod chunk;
mod face;

pub struct Planet {
    max_depth: u32,
    leaf_size: u32,
    faces: [ChunkId; 6],
    chunks: SlotMap<ChunkId, Chunk>,
    sender: Sender<GenerateRequest>,
    receiver: Receiver<GenerateResponse>,
}

impl Planet {
    pub fn new(max_depth: u32, leaf_size: u32, queue: &mut PlanetQueue) -> Self {
        let mut chunks = SlotMap::default();
        let mut faces = create_faces(&mut chunks);
        let (sender, receiver) = start_generator_thread();

        let mut face_idx = 0;
        for (id, chunk) in &chunks {
            let coords = chunk.coords;
            let face = Face::from_index(face_idx);
            let req = GenerateRequest { id, face, coords, max_depth, leaf_size };
            sender.send(req)
                .expect("Error sending generate request for chunk terrain!");
            face_idx += 1;
        }

        Self { max_depth, leaf_size, faces, chunks, sender, receiver }
    }

    pub fn diameter(&self) -> u32 {
        2u32.pow(self.max_depth) * self.leaf_size
    }

    pub fn update(&mut self, anchor: DVec3, queue: &mut PlanetQueue) {
        self.process_responses(queue);
        
        let radius = self.diameter() as f64 * 0.5;
        let mut stack = Vec::with_capacity(self.max_depth as usize);
        for face_idx in 0..6usize {
            let face = Face::from_index(face_idx);
            let anchor = face.get_local_coord(anchor, radius);
            
            stack.push(self.faces[face_idx]);
            while let Some(chunk_id) = stack.pop() {
                let chunk = &self.chunks[chunk_id];
                stack.extend(&chunk.children);
                
            }
        }
    }
    
    fn process_responses(&mut self, queue: &mut PlanetQueue) {
        for res in self.receiver.try_iter() {
            if let Some(chunk) = self.chunks.get(res.request.id) {
                if let None = chunk.children {
                    queue.push_back(PlanetCommand::CreateChunk {
                        id: res.request.id,
                        vertices: res.vertices,
                        indices: res.indices,
                    });
                }
            }
        }
    }
}


pub type PlanetQueue = VecDeque<PlanetCommand>;


pub enum PlanetCommand {
    CreateChunk {
        id: ChunkId,
        vertices: Vec<TerrainVertex>,
        indices: Vec<u32>,
    },
    DestroyChunk {
        id: ChunkId,
    },
}


struct GenerateRequest {
    id: ChunkId,
    face: Face,
    coords: ChunkCoords,
    max_depth: u32,
    leaf_size: u32,
}

struct GenerateResponse {
    request: GenerateRequest,
    vertices: Vec<TerrainVertex>,
    indices: Vec<u32>,
}


fn start_generator_thread() -> (Sender<GenerateRequest>, Receiver<GenerateResponse>) {
    let (req_sx, req_rx) = channel::unbounded();
    let (res_sx, res_rx) = channel::unbounded();
    for thread_idx in 0..1 {
        let req_rx = req_rx.clone();
        let res_sx = res_sx.clone();
        std::thread::Builder::new()
            .name(format!("terrain_generator_{}", thread_idx))
            .spawn(move || {
                while let Ok(req) = req_rx.recv() {
                    let res = generate_chunk_terrain(req);
                    if let Err(_) = res_sx.send(res) {
                        break;
                    } else {
                        println!("Generated new chunk on thread {}!", thread_idx);
                    }
                }
            });
    }
    (req_sx, res_rx)
}

fn create_faces(chunks: &mut SlotMap<ChunkId, Chunk>) -> [ChunkId; 6] {
    let mut faces = [ChunkId::default(); 6];
    for face in &mut faces {
        let coords = ChunkCoords::new(0, 0, 0);
        let chunk = Chunk::new(coords, None);
        let id = chunks.insert(chunk);
        *face = id;
    }
    return faces;
}

fn generate_chunk_terrain(req: GenerateRequest) -> GenerateResponse {
    let scale = 2u32.pow(req.max_depth - req.coords.depth());
    let side_vertices = req.leaf_size + 1;
    let side_cells = req.leaf_size;
    let vertex_count = (side_vertices as usize).pow(2);
    let index_count = ((side_cells as usize).pow(2) * 6);
    let radius = (2u32.pow(req.max_depth) * req.leaf_size) as f64 * 0.5;
    let mut vertices = Vec::with_capacity(vertex_count);
    let mut indices = Vec::with_capacity(index_count);
    for y in 0..side_vertices {
        for x in 0..side_vertices {
            let idx = vertices.len() as u32;
            let is_starting_edge = x == 0 || y == 0;
            let x = ((req.coords.x() + x) * scale) as f64 - radius;
            let y = ((req.coords.y() + y) * scale) as f64 - radius;
            let z = radius;
            vertices.push(TerrainVertex {
                position: [x as _, y as _, z as _],
                color: [1.0, 0.0, 0.0],
            });
            if !is_starting_edge {
                let i0 = idx - 1 - side_vertices as u32;
                let i1 = idx - side_vertices as u32;
                let i2 = idx;
                let i3 = idx - 1;
                indices.extend([i0, i1, i2]);
                indices.extend([i2, i3, i0]);
            }
        }
    }

    let transform = req.face.transform();
    for vertex in &mut vertices {
        let [x, y, z] = vertex.position;
        let (x, y, z) = transform(x, y, z);
        let len = glm::vec3(x, y, z).norm();
        let f = radius / len;
        vertex.position = [x * f, y * f, z * f];
    }

    GenerateResponse {
        request: req,
        vertices,
        indices,
    }
}
