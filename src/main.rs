extern crate nalgebra_glm as glm;

use winit::{
    dpi::PhysicalSize,
    event::{Event, WindowEvent},
    event_loop::ControlFlow,
};

use app::AppEvent;

pub mod app;
pub mod camera;
pub mod graphics;
pub mod linear_allocator;
pub mod loader;
pub mod planet;
pub mod util;


const FULLSCREEN_MODE_EXCLUSIVE: bool = true;

fn main() {
    let event_loop = winit::event_loop::EventLoop::with_user_event();
    let _fs = if FULLSCREEN_MODE_EXCLUSIVE {
        let video_mode = event_loop.primary_monitor()
            .map(|m| m.video_modes().next())
            .flatten()
            .unwrap();
        winit::window::Fullscreen::Exclusive(video_mode)
    } else {
        winit::window::Fullscreen::Borderless(event_loop.primary_monitor())
    }; 
    let window = winit::window::WindowBuilder::new()
        .with_title("Planets")
        .with_inner_size(PhysicalSize::new(640, 480))
        .with_fullscreen(Some(_fs))
        .build(&event_loop)
        .expect("Could not create window!");
    let mut app = app::App::new(window, event_loop.create_proxy());
    event_loop.run(move |event, _, flow| {
        let mut close = false;
        match event {
            Event::MainEventsCleared => app.update(),
            Event::WindowEvent { event: WindowEvent::CloseRequested, .. }
            | Event::UserEvent(AppEvent::Exit) => close = true,
            event => app.event(event),
        }
        *flow = match close {
            false => ControlFlow::Poll,
            true => ControlFlow::Exit,
        };
    });
}
