pub mod mesh;
pub mod renderer;
pub mod shader;
pub mod uniform_block;
pub mod terrain;

mod bind_group;
mod block_data;
mod gpu;
mod indirect;
mod layout_manager;


pub fn rgb(r: f32, g: f32, b: f32) -> wgpu::Color {
    wgpu::Color {
        r: r as _,
        g: g as _,
        b: b as _,
        a: 1.0,
    }
}
