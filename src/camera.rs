use glm::{DVec3, Mat4};

use crate::util;

pub struct Camera {
    pub position: DVec3,
    pub direction: DVec3,
    pub up: DVec3,
    pub tilt: f32,
    pub speed: i32,
    pub sensitivity: f32,
    pub move_forward: bool,
    pub move_back: bool,
    pub move_left: bool,
    pub move_right: bool,
}

impl Camera {
    pub fn new(position: DVec3, direction: DVec3) -> Self {
        Self {
            position,
            direction,
            up: DVec3::z(),
            tilt: 0.0,
            speed: 0,
            sensitivity: 0.1,
            move_forward: false,
            move_back: false,
            move_left: false,
            move_right: false,
        }
    }

    pub fn update(&mut self, delta: f32) {
        let speed = 1.5f64.powi(self.speed) * delta as f64;
        let fb = self.move_forward as i32 as f64 - self.move_back as i32 as f64;
        let lr = self.move_left as i32 as f64 - self.move_right as i32 as f64;
        let forward = self.look_direction() * fb * speed;
        let left = self.left() * lr * speed;
        self.position += forward + left;
    }

    pub fn left(&self) -> DVec3 {
        glm::normalize(&glm::cross(&self.up, &self.direction))
    }

    pub fn look_direction(&self) -> DVec3 {
        let rot = self.tilt.to_radians() as f64;
        glm::rotate_vec3(&self.direction, rot, &self.left())
    }

    pub fn chunk_position(&self) -> DVec3 {
        let chunk_size = 0.1;
        glm::vec3(
            self.position.x % chunk_size,
            self.position.y % chunk_size,
            self.position.z % chunk_size,
        )
    }

    pub fn set_up(&mut self, up: &DVec3) {
        self.up = *up;
        let left = self.left();
        self.direction = glm::normalize(&glm::cross(&left, up));
    }

    pub fn mouse_moved(&mut self, x: f32, y: f32) {
        let x = x * self.sensitivity;
        let y = y * self.sensitivity;
        let rot = -x.to_radians() as f64;
        self.tilt = (self.tilt + y).clamp(-89.9, 89.9);
        self.direction = glm::rotate_vec3(&self.direction, rot, &self.up);
    }

    pub fn mouse_scrolled(&mut self, amount: i32) {
        self.speed += amount;
    }

    pub fn view_projection(&self, aspect: f32) -> Mat4 {
        let center = self.position + self.look_direction();
        let center = util::dvec3_to_vec3(&center);
        let position = util::dvec3_to_vec3(&self.position);
        let up = util::dvec3_to_vec3(&self.up);
        let view = glm::look_at(&position, &center, &up);
        let projection = glm::perspective_rh_zo(aspect, 90.0, 0.1, 5_000_000.0);
        projection * view
    }

    pub fn view_projection_origin(&self, aspect: f32) -> Mat4 {
        let center = self.look_direction();
        let center = util::dvec3_to_vec3(&center);
        let position = &glm::vec3(0.0, 0.0, 0.0);
        let up = util::dvec3_to_vec3(&self.up);
        let view = glm::look_at(&position, &center, &up);
        let projection = glm::perspective_rh_zo(aspect, 90.0, 0.1, 5_000_000.0);
        projection * view
    }
}
