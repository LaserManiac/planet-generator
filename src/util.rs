use std::time::Instant;

pub struct Timer {
    cp: Instant,
}

impl Timer {
    pub fn new() -> Self {
        Self { cp: Instant::now() }
    }

    pub fn reset(&mut self) -> f32 {
        std::mem::replace(&mut self.cp, Instant::now())
            .elapsed()
            .as_secs_f32()
    }
}


pub struct Profiler<'a> {
    label: &'a str,
    start: Instant,
}

impl<'a> Profiler<'a> {
    pub fn start(label: &'a str) -> Self {
        Self {
            label,
            start: Instant::now(),
        }
    }

    pub fn finish(self) {
        let elapsed = self.start.elapsed();
        if elapsed.as_millis() < 10 {
            println!("{}: {}us", self.label, elapsed.as_micros());
        } else {
            println!("{}: {}ms", self.label, elapsed.as_millis());
        }
    }
}


pub fn vec3_to_dvec3(vec: &glm::Vec3) -> glm::DVec3 {
    glm::vec3(vec.x as _, vec.y as _, vec.z as _)
}

pub fn dvec3_to_vec3(vec: &glm::DVec3) -> glm::Vec3 {
    glm::vec3(vec.x as _, vec.y as _, vec.z as _)
}


#[inline]
pub fn offset_of<T, F, I>(field: F) -> usize
    where T: bytemuck::Zeroable,
          F: for<'a> Fn(&'a T) -> &'a I {
    let item = <T as bytemuck::Zeroable>::zeroed();
    let field = field(&item);
    let item_ptr = &item as *const _;
    let field_ptr = field as *const _;
    field_ptr as usize - item_ptr as usize
}
