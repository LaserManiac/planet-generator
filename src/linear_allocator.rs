use slotmap::{new_key_type, SlotMap};

new_key_type! { pub struct Allocation; }


pub struct LinearAllocator {
    capacity: usize,
    allocated: usize,
    free: Vec<Slot>,
    taken: SlotMap<Allocation, Slot>,
}

impl LinearAllocator {
    pub fn new(capacity: usize) -> Self {
        let free = vec![Slot { pos: 0, len: capacity }];
        let taken = SlotMap::default();
        let allocated = 0;
        Self { capacity, free, taken, allocated }
    }

    pub fn alloc(&mut self, size: usize) -> Option<Allocation> {
        // self.free is ascending by size, get first slot that fits
        let idx = self.free.iter().position(|slot| slot.len >= size)?;
        let free_slot = self.free.get_mut(idx)?;

        // Insert allocation
        let handle = self.taken.insert(Slot {
            pos: free_slot.pos,
            len: size,
        });

        // Remove or downsize the free slot
        if free_slot.len == size {
            self.free.remove(idx);
        } else {
            free_slot.len -= size;
            free_slot.pos += size;
            self.sort_free();
        }

        self.allocated += size;

        Some(handle)
    }

    pub fn free(&mut self, handles: &[Allocation]) {
        for handle in handles {
            // TODO: Panic if invalid?
            if let Some(freed) = self.taken.remove(*handle) {
                let start = freed.pos;
                let end = freed.end();
                let mut extended_idx = None;

                // If the freed slot comes right after an existing free slot.
                let existing = self.free.iter_mut()
                    .enumerate()
                    .find(|(_, s)| s.end() == start);
                if let Some((idx, existing)) = existing {
                    // Increase it's length by the length of the freed slot.
                    existing.len += freed.len;
                    // Cache index.
                    extended_idx = Some(idx);
                }
                
                // If the freed slot comes right before an existing free slot.
                let existing = self.free.iter_mut()
                    .enumerate()
                    .find(|(_, s)| s.pos == end);
                if let Some((idx, existing)) = existing {
                    // If there is another overlapping slot.
                    if let Some(extended_idx) = extended_idx {
                        // Extend the first overlapping slot by this slot's size.
                        self.free[extended_idx].len += existing.len;
                        // Remove this slot.
                        self.free.swap_remove(idx);
                    } else {
                        // Move the existing slot to the position of the freed slot.
                        existing.pos = freed.pos;
                        // Increase it's length by the length of the freed slot.
                        existing.len += freed.len;
                        // Cache index.
                        extended_idx = Some(idx);
                    }
                }
                
                // Keep track of allocation.
                self.allocated -= freed.len;
                
                // If this slot is free-standing, add it to the list.
                if extended_idx.is_none() {
                    self.free.push(freed);
                }
            }
        }

        self.sort_free();
    }

    pub fn get(&self, handle: Allocation) -> Option<&Slot> {
        self.taken.get(handle)
    }

    pub fn saturation(&self) -> u32 {
        self.allocated as u32 * 100 / self.capacity as u32
    }

    pub fn fragmentation(&self) -> u32 {
        let remaining = (self.capacity - self.allocated).max(1);
        let sum = self.free.iter()
            .map(|s| s.len)
            .sum::<usize>();
        let avg = sum / self.free.len();
        100 - (avg * 100 / remaining) as u32
    }

    fn sort_free(&mut self) {
        // Sort by len asc, then pos asc
        self.free.sort_by(|a, b| a.len.cmp(&b.len).then(a.pos.cmp(&b.pos)))
    }
}

impl std::ops::Index<Allocation> for LinearAllocator {
    type Output = Slot;

    fn index(&self, index: Allocation) -> &Self::Output {
        self.taken.get(index).expect("Invalid allocation!")
    }
}


pub struct Slot {
    pub pos: usize,
    pub len: usize,
}

impl Slot {
    fn end(&self) -> usize {
        self.pos + self.len
    }
}
