use std::collections::VecDeque;

use slotmap::SecondaryMap;
use winit::{
    event::{
        DeviceEvent,
        ElementState,
        Event,
        KeyboardInput,
        MouseScrollDelta,
        VirtualKeyCode,
        WindowEvent,
    },
    event_loop::EventLoopProxy,
    window::Window,
};

use crate::{
    camera::Camera,
    graphics::{
        renderer::Renderer,
        terrain::{TerrainId, TerrainInstance, TerrainVertex},
    },
    loader::Loader,
    planet::{ChunkCoords, ChunkId, Face, Planet, PlanetCommand, PlanetQueue},
    util::Timer,
};

pub struct App {
    timer: Timer,
    time: f32,
    window: Window,
    focused: bool,
    proxy: EventLoopProxy<AppEvent>,
    loader: Loader,
    renderer: Renderer,
    camera: Camera,
    planet: Planet,
    queue: VecDeque<PlanetCommand>,
    terrain: SecondaryMap<ChunkId, TerrainId>,
}

impl App {
    pub fn new(window: Window, proxy: EventLoopProxy<AppEvent>) -> Self {
        window.set_cursor_grab(true)
            .expect("Could not set cursor grab!");
        window.set_cursor_visible(false);

        let loader = Loader::new("./res");
        let mut queue = PlanetQueue::default();
        let planet = Planet::new(4, 8, &mut queue);
        let camera_pos = glm::vec3(-(planet.diameter() as f64) * 0.5, 0.0, 0.5);
        let camera = Camera::new(camera_pos, glm::DVec3::x());
        let renderer = Renderer::new(&window, &loader);

        Self {
            timer: Timer::new(),
            time: 0.0,
            focused: true,
            window,
            proxy,
            loader,
            renderer,
            camera,
            queue,
            planet,
            terrain: Default::default(),
        }
    }

    pub fn update(&mut self) {
        self.detect_file_changes();

        let delta = self.timer.reset();
        self.time += delta;

        self.planet.update(self.camera.position, &mut self.queue);
        while let Some(cmd) = self.queue.pop_front() {
            match cmd {
                PlanetCommand::CreateChunk { id, vertices, indices } => {
                    let terrain_id = self.renderer.create_terrain(
                        glm::vec3(0.0, 0.0, 0.0),
                        &vertices,
                        &indices,
                        TerrainInstance {
                            transform: glm::Mat4::identity().into(),
                        },
                    ).expect("Could not create chunk terrain!");
                    self.terrain.insert(id, terrain_id);
                }
                PlanetCommand::DestroyChunk { id } => {
                    let terrain_id = self.terrain.remove(id)
                        .expect("Attempted to double-destroy chunk!");
                    self.renderer.remove_terrain(terrain_id);
                }
            }
        }

        self.camera.update(delta);
        self.renderer.render(&self.camera);
    }

    pub fn event(&mut self, event: Event<AppEvent>) {
        match event {
            Event::WindowEvent { event: WindowEvent::Focused(focus), .. } =>
                self.focused = focus,
            Event::WindowEvent { event: WindowEvent::Resized(size), .. } =>
                self.resize_event(size.width, size.height),
            Event::WindowEvent { event: WindowEvent::KeyboardInput { input, .. }, .. } =>
                self.keyboard_event(input),
            Event::WindowEvent { event: WindowEvent::MouseWheel { delta, .. }, .. } =>
                self.mouse_input_scroll(delta),
            Event::DeviceEvent { event: DeviceEvent::MouseMotion { delta: (x, y), .. }, .. } => {
                if self.focused {
                    self.camera.mouse_moved(x as _, y as _)
                }
            }
            _ => (),
        }
    }

    fn resize_event(&mut self, width: u32, height: u32) {
        self.renderer.resize(width, height);
    }

    fn keyboard_event(&mut self, event: KeyboardInput) {
        if let Some(VirtualKeyCode::Escape) = event.virtual_keycode {
            if let Err(_) = self.proxy.send_event(AppEvent::Exit) {
                panic!("Event loop closed!");
            }
        }

        if let Some(VirtualKeyCode::Space) = event.virtual_keycode {
            if let ElementState::Pressed = event.state {
                let terrain_wireframe = self.renderer.terrain_wireframe();
                self.renderer.set_terrain_wireframe(!terrain_wireframe);
            }
        }

        let pressed = event.state == ElementState::Pressed;
        match event.virtual_keycode {
            Some(VirtualKeyCode::W) => self.camera.move_forward = pressed,
            Some(VirtualKeyCode::S) => self.camera.move_back = pressed,
            Some(VirtualKeyCode::A) => self.camera.move_left = pressed,
            Some(VirtualKeyCode::D) => self.camera.move_right = pressed,
            _ => (),
        }
    }

    fn mouse_input_scroll(&mut self, delta: MouseScrollDelta) {
        match delta {
            MouseScrollDelta::LineDelta(.., y) => self.camera.mouse_scrolled(y as i32),
            MouseScrollDelta::PixelDelta(pos) => self.camera.mouse_scrolled(pos.y as i32),
        }
    }

    fn detect_file_changes(&mut self) {
        for changed in self.loader.changed() {
            let changed = changed.to_string_lossy();
            if changed.ends_with("terrain.wgsl") {
                // TODO: self.gfx.reload_planet_shader(&self.loader);
            } else {
                println!("Unhandled file changed: {}", changed);
            }
        }
    }
}


pub enum AppEvent {
    Exit
}
