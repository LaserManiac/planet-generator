use std::{
    any::TypeId,
    collections::HashMap,
    sync::Arc,
};

use wgpu::{BindGroupLayout, Device};

use crate::graphics::{
    bind_group::BindGroupData,
    block_data::BlockData,
};

pub struct LayoutManager {
    device: Arc<wgpu::Device>,
    bind_group: HashMap<TypeId, BindGroupLayout>,
    batch_bind_group: HashMap<TypeId, BindGroupLayout>,
}

impl LayoutManager {
    pub fn new(device: Arc<wgpu::Device>) -> Self {
        Self {
            device,
            bind_group: Default::default(),
            batch_bind_group: Default::default(),
        }
    }

    pub fn register_bind_group<T: BindGroupData + 'static>(&mut self) {
        let type_id = std::any::TypeId::of::<T>();
        if let None = self.bind_group.get(&type_id) {
            let layout = self.device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some(std::any::type_name::<T>()),
                entries: &<T as BindGroupData>::layout_entries(),
            });
            self.bind_group.insert(type_id, layout);
        }
    }

    pub fn register_batch_bind_group<Block, Instance>(&mut self)
        where Block: BindGroupData + 'static,
              Instance: BlockData + 'static {
        let type_id = std::any::TypeId::of::<(Block, Instance)>();
        if let None = self.batch_bind_group.get(&type_id) {
            let mut entries = <Block as BindGroupData>::layout_entries();
            let instance_entry = <Instance as BlockData>::layout_entry(entries.len() as _);
            entries.push(instance_entry);
            let descriptor = wgpu::BindGroupLayoutDescriptor {
                label: Some(std::any::type_name::<(Block, Instance)>()),
                entries: &entries,
            };
            let layout = self.device.create_bind_group_layout(&descriptor);
            self.batch_bind_group.insert(type_id, layout);
        }
    }

    pub fn bind_group_layout<T: BindGroupData + 'static>(&self) -> &BindGroupLayout {
        let type_id = std::any::TypeId::of::<T>();
        self.bind_group.get(&type_id)
            .ok_or_else(|| std::any::type_name::<T>())
            .expect("Missing layout!")
    }

    pub fn batch_bind_group_layout<Block, Instance>(&self) -> &BindGroupLayout
        where Block: BindGroupData + 'static,
              Instance: BlockData + 'static {
        let type_id = std::any::TypeId::of::<(Block, Instance)>();
        self.batch_bind_group.get(&type_id)
            .ok_or_else(|| std::any::type_name::<(Block, Instance)>())
            .expect("Missing layout!")
    }
}
