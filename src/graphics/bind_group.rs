use crate::graphics::layout_manager::LayoutManager;

pub trait BindGroupData: 'static {
    fn layout_entries() -> Vec<wgpu::BindGroupLayoutEntry>;
    fn entries(&self) -> Vec<wgpu::BindGroupEntry>;
    fn flush(&mut self, queue: &wgpu::Queue);
}


pub struct BindGroup<T: BindGroupData> {
    data: T,
    bind_group: wgpu::BindGroup,
}

impl<T: BindGroupData> BindGroup<T> {
    pub fn new(data: T, layout_manager: &LayoutManager, device: &wgpu::Device) -> Self {
        let bind_group = Self::create_bind_group(&data, layout_manager, device);
        Self { data, bind_group }
    }

    pub fn recreate(&mut self, layout_manager: &LayoutManager, device: &wgpu::Device) {
        self.bind_group = Self::create_bind_group(&self.data, layout_manager, device);
    }

    pub fn get(&self) -> &wgpu::BindGroup {
        &self.bind_group
    }
    
    fn create_bind_group(
        data: &T,
        layout_manager: &LayoutManager,
        device: &wgpu::Device,
    ) -> wgpu::BindGroup {
        device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some(std::any::type_name::<T>()),
            layout: layout_manager.bind_group_layout::<T>(),
            entries: &data.entries(),
        })
    }
}

impl<T: BindGroupData> std::ops::Deref for BindGroup<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl<T: BindGroupData> std::ops::DerefMut for BindGroup<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}
