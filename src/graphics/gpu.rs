use std::sync::Arc;

use winit::window::Window;

pub struct Gpu {
    pub surface: wgpu::Surface,
    pub device: Arc<wgpu::Device>,
    pub queue: Arc<wgpu::Queue>,
    pub swap_chain: wgpu::SwapChain,
}

impl Gpu {
    pub fn new(window: &Window) -> Result<Self, String> {
        futures::executor::block_on(Self::new_async(window))
    }
    
    async fn new_async(window: &Window) -> Result<Self, String> {
        let instance = wgpu::Instance::new(wgpu::BackendBit::PRIMARY);
        let surface = unsafe { instance.create_surface(window) };
        
        let adapter_options = wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::HighPerformance,
            compatible_surface: Some(&surface),
        };
        let device_descriptor = wgpu::DeviceDescriptor {
            label: Some("main_device"),
            features: wgpu::Features::MULTI_DRAW_INDIRECT | wgpu::Features::NON_FILL_POLYGON_MODE,
            limits: wgpu::Limits::default(),
        };
        let trace_path = std::path::Path::new("./wgpu_trace.txt");
        
        let adapter = instance.request_adapter(&adapter_options)
            .await
            .ok_or_else(|| String::from("Could not get surface-compatible WGPU adapter!"))?;
        let (device, queue) = adapter.request_device(&device_descriptor, Some(trace_path))
            .await
            .map_err(|err| format!("Could not create WGPU device: {}", err))?;
        device.on_uncaptured_error(|err| panic!("[WGPU] {}", err));
        let size = window.inner_size();
        let swap_chain = Self::create_swap_chain(size.width, size.height, &surface, &device);
        Ok(Self {
            surface,
            device: Arc::new(device),
            queue: Arc::new(queue),
            swap_chain
        })
    }
    
    pub fn resize(&mut self, width: u32, height: u32) {
        self.swap_chain = Self::create_swap_chain(width, height, &self.surface, &self.device);
    }
    
    pub fn next_frame(&mut self) -> wgpu::SwapChainFrame {
        self.swap_chain.get_current_frame()
            .expect("Could not get next swap chain frame!")
    }

    fn create_swap_chain(
        width: u32,
        height: u32,
        surface: &wgpu::Surface,
        device: &wgpu::Device,
    ) -> wgpu::SwapChain {
        device.create_swap_chain(surface, &wgpu::SwapChainDescriptor {
            usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
            format: wgpu::TextureFormat::Bgra8Unorm,
            width,
            height,
            present_mode: wgpu::PresentMode::Immediate,
        })
    }
}
