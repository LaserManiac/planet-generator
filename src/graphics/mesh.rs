use glm::Mat4;

pub mod allocator;
pub mod batch;
pub mod renderer;


pub trait IndexData: bytemuck::Pod {
    const FORMAT: wgpu::IndexFormat;

    fn from_usize(value: usize) -> Self;
    fn into_usize(self) -> usize;
}

impl IndexData for u32 {
    const FORMAT: wgpu::IndexFormat = wgpu::IndexFormat::Uint32;

    fn from_usize(value: usize) -> Self {
        value as Self
    }

    fn into_usize(self) -> usize {
        self as usize
    }
}


pub trait VertexData: bytemuck::Pod {
    fn buffer_layout() -> &'static wgpu::VertexBufferLayout<'static>;
}


pub type MeshIndexPod = u32;


#[derive(Copy, Clone)]
#[repr(C)]
pub struct MeshVertexPod {
    pub pos: [f32; 3],
    pub nor: [f32; 3],
    pub col: [f32; 3],
    pub mag: f32,
}

impl MeshVertexPod {
    pub fn buffer_layout() -> wgpu::VertexBufferLayout<'static> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Self>() as _,
            step_mode: wgpu::InputStepMode::Vertex,
            attributes: &[
                // Position
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Float32x3,
                    offset: 0,
                    shader_location: 0,
                },
                // Normal
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Float32x3,
                    offset: 3 * 4,
                    shader_location: 1,
                },
                // Color
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Float32x3,
                    offset: 6 * 4,
                    shader_location: 2,
                },
                // Magnitude
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Float32,
                    offset: 9 * 4,
                    shader_location: 3,
                },
            ],
        }
    }
}

unsafe impl bytemuck::Zeroable for MeshVertexPod {}

unsafe impl bytemuck::Pod for MeshVertexPod {}


#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct MeshUniformsPod {
    pub view_projection: [[f32; 4]; 4],
    pub camera_position: [f32; 4],
    pub light: [f32; 3],
    pub time: f32,
}

unsafe impl bytemuck::Zeroable for MeshUniformsPod {}

unsafe impl bytemuck::Pod for MeshUniformsPod {}


#[repr(C)]
#[derive(Copy, Clone)]
pub struct MeshInstancePod {
    pub transform: Mat4,
}

unsafe impl bytemuck::Zeroable for MeshInstancePod {}

unsafe impl bytemuck::Pod for MeshInstancePod {}
