pub trait BlockData: bytemuck::Pod + 'static {
    const BINDING_TYPE: wgpu::BufferBindingType;

    fn layout_entry(binding: u32) -> wgpu::BindGroupLayoutEntry {
        wgpu::BindGroupLayoutEntry {
            binding,
            visibility: wgpu::ShaderStage::VERTEX_FRAGMENT,
            ty: wgpu::BindingType::Buffer {
                ty: Self::BINDING_TYPE,
                has_dynamic_offset: false,
                min_binding_size: None, // wgpu::BufferSize::new(std::mem::size_of::<Self>() as _),
            },
            count: None,
        }
    }

    fn entry(binding: u32, buffer: &wgpu::Buffer) -> wgpu::BindGroupEntry {
        wgpu::BindGroupEntry {
            binding,
            resource: buffer.as_entire_binding(),
        }
    }
}
