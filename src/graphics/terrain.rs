use wgpu::BindGroupLayoutDescriptor;

use crate::graphics::{
    bind_group::BindGroupData,
    block_data::BlockData,
    layout_manager::LayoutManager,
    mesh::{
        batch::MeshInstanceId,
        VertexData,
    },
    uniform_block::UniformBlock,
};

#[derive(Copy, Clone, Hash)]
pub struct TerrainId(pub(super) MeshInstanceId);


#[derive(Copy, Clone)]
#[repr(C)]
pub struct TerrainVertex {
    pub position: [f32; 3],
    pub color: [f32; 3],
}

impl VertexData for TerrainVertex {
    fn buffer_layout() -> &'static wgpu::VertexBufferLayout<'static> {
        &wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Self>() as _,
            step_mode: wgpu::InputStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Float32x3,
                    offset: 0,
                    shader_location: 0,
                },
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Float32x3,
                    offset: 12,
                    shader_location: 1,
                },
            ],
        }
    }
}

unsafe impl bytemuck::Zeroable for TerrainVertex {}

unsafe impl bytemuck::Pod for TerrainVertex {}


pub struct TerrainBatch {
    pub block: UniformBlock<TerrainBatchData>,
}

impl TerrainBatch {
    pub fn new(block_data: TerrainBatchData, device: &wgpu::Device) -> Self {
        let block = UniformBlock::new(block_data, device);
        Self { block }
    }
}

impl BindGroupData for TerrainBatch {
    fn layout_entries() -> Vec<wgpu::BindGroupLayoutEntry> {
        vec![
            TerrainBatchData::layout_entry(0),
        ]
    }

    fn entries(&self) -> Vec<wgpu::BindGroupEntry> {
        vec![
            self.block.entry(0),
        ]
    }

    fn flush(&mut self, queue: &wgpu::Queue) {
        self.block.flush(queue);
    }
}


#[derive(Copy, Clone, Default)]
#[repr(C)]
pub struct TerrainBatchData {
    pub camera_position: [f32; 3],
    pub time: f32,
}

impl BlockData for TerrainBatchData {
    const BINDING_TYPE: wgpu::BufferBindingType = wgpu::BufferBindingType::Uniform;
}

unsafe impl bytemuck::Zeroable for TerrainBatchData {}

unsafe impl bytemuck::Pod for TerrainBatchData {}


#[derive(Copy, Clone)]
#[repr(C)]
pub struct TerrainInstance {
    pub transform: [[f32; 4]; 4],
}

impl BlockData for TerrainInstance {
    const BINDING_TYPE: wgpu::BufferBindingType = wgpu::BufferBindingType::Storage {
        read_only: true,
    };
}

unsafe impl bytemuck::Zeroable for TerrainInstance {}

unsafe impl bytemuck::Pod for TerrainInstance {}
