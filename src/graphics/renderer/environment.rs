use crate::graphics::{
    bind_group::BindGroupData,
    block_data::BlockData,
    layout_manager::LayoutManager,
    uniform_block::UniformBlock,
};

#[derive(Copy, Clone, Default)]
#[repr(C)]
pub struct EnvironmentBlock {
    pub view_projection: [[f32; 4]; 4],
    pub sun_direction: [f32; 3],
}

impl BlockData for EnvironmentBlock {
    const BINDING_TYPE: wgpu::BufferBindingType = wgpu::BufferBindingType::Uniform;
}

unsafe impl bytemuck::Zeroable for EnvironmentBlock {}

unsafe impl bytemuck::Pod for EnvironmentBlock {}


pub struct Environment {
    pub block: UniformBlock<EnvironmentBlock>,
}

impl Environment {
    pub fn new(environment: EnvironmentBlock, device: &wgpu::Device) -> Self {
        Self {
            block: UniformBlock::new(environment, device),
        }
    }
}

impl BindGroupData for Environment {
    fn layout_entries() -> Vec<wgpu::BindGroupLayoutEntry> {
        vec![
            EnvironmentBlock::layout_entry(0),
        ]
    }

    fn entries(&self) -> Vec<wgpu::BindGroupEntry> {
        vec![
            self.block.entry(0),
        ]
    }

    fn flush(&mut self, queue: &wgpu::Queue) {
        self.block.flush(queue);
    }
}
