use wgpu::util::{BufferInitDescriptor, DeviceExt as _};

use crate::graphics::block_data::BlockData;


pub struct UniformBlock<T: BlockData> {
    buffer: wgpu::Buffer,
    data: T,
    dirty: bool,
}

impl<T: BlockData> UniformBlock<T> {
    pub fn new(data: T, device: &wgpu::Device) -> Self {
        let buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Uniform buffer"),
            usage: wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
            contents: bytemuck::cast_slice(&[data]),
        });
        let dirty = false;
        Self { data, buffer, dirty }
    }

    pub fn flush(&mut self, queue: &wgpu::Queue) {
        if !self.dirty {
            return;
        }

        let slice = std::slice::from_ref(&self.data);
        let data = bytemuck::cast_slice(slice);
        queue.write_buffer(&self.buffer, 0, data);

        self.dirty = false;
    }

    pub fn get(&self) -> T {
        self.data
    }

    pub fn set(&mut self, data: T) {
        self.data = data;
        self.dirty = true;
    }

    pub fn entry(&self, binding: u32) -> wgpu::BindGroupEntry {
        <T as BlockData>::entry(binding, &self.buffer)
    }
}
