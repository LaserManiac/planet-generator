use std::{
    borrow::Cow,
    collections::{HashMap, HashSet},
    sync::Arc,
};

use slotmap::{new_key_type, SecondaryMap, SlotMap};
use wgpu::{Device, ShaderFlags, ShaderModule, ShaderModuleDescriptor, ShaderSource};

use crate::loader::Loader;

new_key_type! { pub struct Shader; }


pub struct ShaderManager {
    device: Arc<Device>,
    shader_names: SlotMap<Shader, String>,
    name_shaders: HashMap<String, Shader>,
    modules: SecondaryMap<Shader, ShaderModule>,
    swapped: HashSet<Shader>,
}

impl ShaderManager {
    pub fn new(device: Arc<Device>) -> Self {
        Self {
            device,
            shader_names: Default::default(),
            name_shaders: Default::default(),
            modules: Default::default(),
            swapped: Default::default(),
        }
    }

    pub fn create_or_swap(&mut self, name: &str, loader: &Loader) -> Shader {
        let (shader, swap) = self.alloc_handle(name);
        let source = match loader.load_to_string(name) {
            Ok(source) => source,
            Err(err) => {
                println!("[WARNING] Could not load shader source '{}': {}", name, err);
                return shader;
            }
        };
        match self.create_shader_module(name, source) {
            Ok(module) => {
                self.modules.insert(shader, module);
                if swap {
                    self.swapped.insert(shader);
                }
            }
            Err(err) => println!("[WARNING] Could not create shader '{}': {}", name, err),
        }
        return shader;
    }

    pub fn get_module(&self, shader: Shader) -> Option<&ShaderModule> {
        self.modules.get(shader)
    }

    pub fn swapped_iter<'a>(&'a mut self) -> impl Iterator<Item=Shader> + 'a {
        self.swapped.drain()
    }

    fn alloc_handle(&mut self, name: &str) -> (Shader, bool) {
        match self.name_shaders.get(name) {
            Some(shader) => (*shader, true),
            None => {
                let shader = self.shader_names.insert(name.to_string());
                self.name_shaders.insert(name.to_string(), shader);
                (shader, false)
            }
        }
    }

    fn create_shader_module(&self, name: &str, source: String) -> Result<ShaderModule, String> {
        let device = Arc::clone(&self.device);
        let name_owned = name.to_string();
        let loader = move || {
            device.create_shader_module(&ShaderModuleDescriptor {
                label: Some(&name_owned),
                source: ShaderSource::Wgsl(std::borrow::Cow::Owned(source)),
                flags: ShaderFlags::VALIDATION | ShaderFlags::EXPERIMENTAL_TRANSLATION,
            })
        };

        // Spawn a new thread because create_shader_module panics instead of returning a Result.
        // If the thread panics, an Err is returned.
        std::thread::Builder::new()
            .name(String::from("shader_loader"))
            .spawn(loader)
            .map_err(|err| format!("Could not spawn shader loader thread: {}", err))?
            .join()
            .map_err(|_| format!("Could not load shader '{}'!", name))
    }
}
