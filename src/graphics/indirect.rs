#[derive(Copy, Clone)]
#[repr(C)]
pub struct DrawIndexedIndirect {
    // The number of vertices to draw.
    pub vertex_count: u32,
    // The number of instances to draw.
    pub instance_count: u32,
    // The base index within the index buffer.
    pub base_index: u32,
    // The value added to the vertex index before indexing into the vertex buffer.
    pub vertex_offset: i32,
    // The instance ID of the first instance to draw.
    pub base_instance: u32,
}

unsafe impl bytemuck::Zeroable for DrawIndexedIndirect {}

unsafe impl bytemuck::Pod for DrawIndexedIndirect {}
