use crate::graphics::{
    bind_group::{BindGroup, BindGroupData},
    block_data::BlockData,
    layout_manager::LayoutManager,
    mesh::{
        {IndexData, VertexData},
        allocator::MeshAllocator,
        batch::MeshBatch,
    },
    shader::{Shader, ShaderManager},
};

pub struct MeshRenderer<Vertex, EnvironmentData, BatchData, InstanceData>
    where Vertex: VertexData,
          EnvironmentData: BindGroupData,
          BatchData: BindGroupData,
          InstanceData: BlockData {
    shader: Shader,
    pipeline_layout: wgpu::PipelineLayout,
    pipeline: Option<wgpu::RenderPipeline>,
    wireframe: bool,
    dirty: bool,
    _marker: std::marker::PhantomData<(
            for<'a, 'b, 'c, 'd> fn(
            &'a [Vertex],
            &'b EnvironmentData,
            &'c BatchData,
            &'d [InstanceData],
        ),
    )>,
}

impl<Vertex, EnvironmentData, BatchData, InstanceData>
MeshRenderer<Vertex, EnvironmentData, BatchData, InstanceData>
    where Vertex: VertexData,
          EnvironmentData: BindGroupData,
          BatchData: BindGroupData,
          InstanceData: BlockData {
    pub fn new(
        shader: Shader,
        shader_manager: &ShaderManager,
        layout_manager: &LayoutManager,
        device: &wgpu::Device,
    ) -> Self {
        let mut renderer = Self {
            shader,
            pipeline_layout: Self::create_pipeline_layout(device, layout_manager),
            pipeline: None,
            wireframe: false,
            dirty: true,
            _marker: Default::default(),
        };
        renderer.manage_state(device, shader_manager);
        return renderer;
    }

    pub fn set_dirty(&mut self) {
        self.dirty = true;
    }

    pub fn wireframe(&self) -> bool {
        self.wireframe
    }

    pub fn set_wireframe(&mut self, wireframe: bool) {
        self.dirty |= self.wireframe != wireframe;
        self.wireframe = wireframe;
    }

    pub fn shader(&self) -> Shader {
        self.shader
    }

    pub fn set_shader(&mut self, shader: Shader) {
        self.dirty |= self.shader != shader;
        self.shader = shader;
    }

    pub fn manage_state(&mut self, device: &wgpu::Device, shaders: &ShaderManager) {
        if !self.dirty {
            return;
        }

        if let Some(shader_module) = shaders.get_module(self.shader) {
            self.pipeline = Self::create_pipeline(
                device,
                &self.pipeline_layout,
                shader_module,
                self.wireframe,
            ).into();
        }

        self.dirty = false;
    }

    pub fn render<'a, 'b: 'a, Index: IndexData>(
        &'b self,
        environment_uniforms: &'b BindGroup<EnvironmentData>,
        batch: &'b MeshBatch<BatchData, InstanceData>,
        allocator: &'b MeshAllocator<Vertex, Index>,
        pass: &mut wgpu::RenderPass<'a>,
    ) {
        if let Some(pipeline) = &self.pipeline {
            pass.set_pipeline(pipeline);
            pass.set_vertex_buffer(0, allocator.vertex_buffer.slice(..));
            pass.set_index_buffer(allocator.index_buffer.slice(..), Index::FORMAT);
            pass.set_bind_group(0, environment_uniforms.get(), &[]);
            pass.set_bind_group(1, &batch.bind_group, &[]);
            pass.multi_draw_indexed_indirect(&batch.indirect_buffer, 0, batch.instance_count());
        }
    }

    fn create_pipeline_layout(
        device: &wgpu::Device,
        layout_manager: &LayoutManager,
    ) -> wgpu::PipelineLayout {
        let environment_layout = layout_manager.bind_group_layout::<EnvironmentData>();
        let batch_layout = layout_manager.batch_bind_group_layout::<BatchData, InstanceData>();
        device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("mesh_renderer_pipeline_layout"),
            bind_group_layouts: &[
                &environment_layout,
                &batch_layout,
            ],
            push_constant_ranges: &[],
        })
    }

    fn create_pipeline(
        device: &wgpu::Device,
        pipeline_layout: &wgpu::PipelineLayout,
        shader: &wgpu::ShaderModule,
        wireframe: bool,
    ) -> wgpu::RenderPipeline {
        device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("mesh_renderer_pipeline_descriptor"),
            layout: Some(pipeline_layout),
            vertex: wgpu::VertexState {
                module: shader,
                entry_point: "vertex_main",
                buffers: std::slice::from_ref(Vertex::buffer_layout()),
            },
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                polygon_mode: match wireframe {
                    true => wgpu::PolygonMode::Line,
                    false => wgpu::PolygonMode::Fill,
                },
                clamp_depth: false,
                conservative: false,
            },
            depth_stencil: Some(wgpu::DepthStencilState {
                format: wgpu::TextureFormat::Depth32Float,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::LessEqual,
                stencil: Default::default(),
                bias: Default::default(),
            }),
            multisample: Default::default(),
            fragment: Some(wgpu::FragmentState {
                module: shader,
                entry_point: "fragment_main",
                targets: &[
                    wgpu::ColorTargetState {
                        format: wgpu::TextureFormat::Bgra8Unorm,
                        write_mask: wgpu::ColorWrite::ALL,
                        blend: Some(wgpu::BlendState::REPLACE),
                    }
                ],
            }),
        })
    }
}
