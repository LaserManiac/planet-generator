use slotmap::{Key, new_key_type, SecondaryMap, SlotMap};

use crate::graphics::{
    bind_group::BindGroupData,
    block_data::BlockData,
    indirect::DrawIndexedIndirect,
    layout_manager::LayoutManager,
    mesh::{
        {IndexData, VertexData},
        allocator::{Mesh, MeshAllocator},
    },
    uniform_block::UniformBlock,
};

new_key_type! { pub struct MeshInstanceId; }


pub struct MeshBatch<BatchData: BindGroupData, InstanceData: BlockData> {
    pub(super) bind_group: wgpu::BindGroup,
    pub(super) indirect_buffer: wgpu::Buffer,
    capacity: usize,
    batch_data: BatchData,
    instance_buffer: wgpu::Buffer,
    instance_local: Box<[InstanceData]>,
    indirect_local: Box<[DrawIndexedIndirect]>,
    instance_dirty: bool,
    indirect_dirty: bool,
    instances: SlotMap<MeshInstanceId, Mesh>,
    instance_indices: SecondaryMap<MeshInstanceId, usize>,
    index_instances: Box<[MeshInstanceId]>,
}

impl<BatchData: BindGroupData, InstanceData: BlockData> MeshBatch<BatchData, InstanceData> {
    pub fn new(
        capacity: usize,
        batch_data: BatchData,
        layout_manager: &LayoutManager,
        device: &wgpu::Device,
    ) -> Self {
        let instance_size = std::mem::size_of::<InstanceData>();
        let instance_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("mesh_batch_instance_buffer"),
            size: (instance_size * capacity) as _,
            usage: wgpu::BufferUsage::STORAGE | wgpu::BufferUsage::COPY_DST,
            mapped_at_creation: false,
        });

        let indirect_size = std::mem::size_of::<DrawIndexedIndirect>();
        let indirect_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("mesh_batch_indirect_buffer"),
            size: (indirect_size * capacity) as _,
            usage: wgpu::BufferUsage::INDIRECT | wgpu::BufferUsage::COPY_DST,
            mapped_at_creation: false,
        });

        let bind_group = {
            let mut entries = batch_data.entries();
            let final_entry = InstanceData::entry(entries.len() as _, &instance_buffer);
            entries.push(final_entry);
            device.create_bind_group(&wgpu::BindGroupDescriptor {
                label: Some(std::any::type_name::<Self>()),
                layout: &layout_manager.batch_bind_group_layout::<BatchData, InstanceData>(),
                entries: &entries,
            })
        };

        let instance_local = vec![bytemuck::Zeroable::zeroed(); capacity].into_boxed_slice();
        let instance_dirty = false;

        let indirect_local = vec![bytemuck::Zeroable::zeroed(); capacity].into_boxed_slice();
        let indirect_dirty = false;

        let instances = SlotMap::with_capacity_and_key(capacity);
        let instance_indices = SecondaryMap::with_capacity(capacity);
        let index_instances = vec![MeshInstanceId::null(); capacity].into_boxed_slice();

        Self {
            batch_data,
            capacity,
            instance_buffer,
            bind_group,
            instance_local,
            instance_dirty,
            indirect_buffer,
            indirect_local,
            indirect_dirty,
            instances,
            instance_indices,
            index_instances,
        }
    }

    pub fn batch_data(&self) -> &BatchData {
        &self.batch_data
    }

    pub fn batch_data_mut(&mut self) -> &mut BatchData {
        &mut self.batch_data
    }

    pub fn create_instance<Vertex: VertexData, Index: IndexData>(
        &mut self,
        mesh: Mesh,
        instance_data: InstanceData,
        allocator: &MeshAllocator<Vertex, Index>,
    ) -> Option<MeshInstanceId> {
        if self.instances.len() == self.capacity {
            return None;
        }

        let idx = self.instances.len();
        let instance = self.instances.insert(mesh);
        self.instance_indices.insert(instance, idx);
        self.index_instances[idx] = instance;

        let indirect = DrawIndexedIndirect {
            base_instance: idx as _,
            ..allocator.get_draw_indexed_indirect(mesh)?
        };

        self.instance_local[idx] = instance_data;
        self.instance_dirty = true;

        self.indirect_local[idx] = indirect;
        self.indirect_dirty = true;

        Some(instance)
    }

    pub fn destroy_instance(&mut self, instance: MeshInstanceId) -> Mesh {
        let mesh = self.instances.remove(instance)
            .expect("Attempted to double-delete instance!");
        let remove_idx = self.instance_indices.remove(instance).unwrap();
        self.index_instances[remove_idx] = MeshInstanceId::null();

        if remove_idx != self.instances.len() {
            let swap_idx = self.instances.len();
            let last_instance = self.index_instances[swap_idx];

            self.instance_indices.insert(last_instance, remove_idx);
            self.index_instances.swap(remove_idx, swap_idx);

            self.instance_local.swap(remove_idx, swap_idx);
            self.instance_dirty = true;

            self.indirect_local.swap(remove_idx, swap_idx);
            self.indirect_local[remove_idx].base_instance = remove_idx as _;
            self.indirect_dirty = true;
        }
        
        return mesh;
    }

    pub fn instance(&self, instance: MeshInstanceId) -> Option<&InstanceData> {
        let idx = self.instance_indices.get(instance)?;
        self.instance_local.get(*idx)
    }

    pub fn set_instance(&mut self, instance: MeshInstanceId, pod: InstanceData) {
        let idx = self.instance_indices[instance];
        self.instance_local[idx] = pod;
        self.instance_dirty = true;
    }

    pub fn instance_count(&self) -> u32 {
        self.instances.len() as _
    }

    pub fn flush(&mut self, queue: &wgpu::Queue) {
        self.batch_data.flush(queue);

        if self.instance_dirty {
            let data = bytemuck::cast_slice(&self.instance_local);
            queue.write_buffer(&self.instance_buffer, 0, data);
            self.instance_dirty = false;
            //println!("Flush mesh batch instance buffer!")
        }

        if self.indirect_dirty {
            let data = bytemuck::cast_slice(&self.indirect_local);
            queue.write_buffer(&self.indirect_buffer, 0, data);
            self.indirect_dirty = false;
            //println!("Flush mesh batch indirect buffer!");
        }
    }
}
