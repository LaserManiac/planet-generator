use slotmap::{new_key_type, SlotMap};

use crate::{
    graphics::{
        indirect::DrawIndexedIndirect,
        mesh::{IndexData, VertexData},
    },
    linear_allocator::{Allocation, LinearAllocator},
};

new_key_type! { pub struct Mesh; }


pub struct MeshAllocator<V: VertexData, I: IndexData> {
    pub(super) vertex_buffer: wgpu::Buffer,
    pub(super) index_buffer: wgpu::Buffer,
    vertex_allocator: LinearAllocator,
    index_allocator: LinearAllocator,
    meshes: SlotMap<Mesh, MeshData>,
    _marker: (std::marker::PhantomData<[V]>, std::marker::PhantomData<[I]>),
}

impl<V: VertexData, I: IndexData> MeshAllocator<V, I> {
    pub fn new(vertex_capacity: usize, index_capacity: usize, device: &wgpu::Device) -> Self {
        let vertex_allocator = LinearAllocator::new(vertex_capacity);
        let vertex_buffer_size = vertex_capacity * std::mem::size_of::<V>();
        let vertex_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("mesh_allocator_vertex_buffer"),
            size: vertex_buffer_size as _,
            usage: wgpu::BufferUsage::VERTEX | wgpu::BufferUsage::COPY_DST,
            mapped_at_creation: false,
        });

        let index_allocator = LinearAllocator::new(index_capacity);
        let index_buffer_size = index_capacity * std::mem::size_of::<I>();
        let index_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("mesh_allocator_index_buffer"),
            size: index_buffer_size as _,
            usage: wgpu::BufferUsage::INDEX | wgpu::BufferUsage::COPY_DST,
            mapped_at_creation: false,
        });

        println!(
            "New mesh allocator\n\tVertex capacity: {} [{} Mib]\n\tIndex capacity: {} [{} Mib]",
            vertex_capacity,
            vertex_buffer_size / 1024 / 1024,
            index_capacity,
            index_buffer_size / 1024 / 1024,
        );

        Self {
            vertex_buffer,
            vertex_allocator,
            index_buffer,
            index_allocator,
            meshes: Default::default(),
            _marker: Default::default(),
        }
    }

    pub fn alloc(&mut self, vertices: &[V], indices: &[I], queue: &wgpu::Queue) -> Option<Mesh> {
        let vertex_allocation = self.vertex_allocator.alloc(vertices.len())?;
        let index_allocation = match self.index_allocator.alloc(indices.len()) {
            Some(index_allocation) => index_allocation,
            None => {
                self.vertex_allocator.free(&[vertex_allocation]);
                return None;
            }
        };

        let vertex_size = std::mem::size_of::<V>();
        let vertex_pos = self.vertex_allocator[vertex_allocation].pos * vertex_size;
        queue.write_buffer(&self.vertex_buffer, vertex_pos as _, bytemuck::cast_slice(vertices));

        let index_size = std::mem::size_of::<I>();
        let index_pos = self.index_allocator[index_allocation].pos * index_size;
        queue.write_buffer(&self.index_buffer, index_pos as _, bytemuck::cast_slice(indices));

        Some(self.meshes.insert(MeshData {
            vertices: vertex_allocation,
            indices: index_allocation,
        }))
    }

    pub fn free(&mut self, mesh: Mesh) {
        let data = self.meshes.remove(mesh)
            .expect("Attempted to double-free mesh!");
        self.vertex_allocator.free(&[data.vertices]);
        self.index_allocator.free(&[data.indices]);
    }

    pub fn stats(&self) -> MeshAllocatorStats {
        let vertex_saturation = self.vertex_allocator.saturation();
        let index_saturation = self.vertex_allocator.saturation();
        let vertex_fragmentation = self.vertex_allocator.fragmentation();
        let index_fragmentation = self.vertex_allocator.fragmentation();
        MeshAllocatorStats {
            vertex_saturation,
            index_saturation,
            vertex_fragmentation,
            index_fragmentation,
        }
    }

    pub(super) fn get_draw_indexed_indirect(&self, mesh: Mesh) -> Option<DrawIndexedIndirect> {
        let data = self.meshes.get(mesh)?;
        let vertex_slot = self.vertex_allocator.get(data.vertices)?;
        let index_slot = self.index_allocator.get(data.indices)?;
        Some(DrawIndexedIndirect {
            vertex_offset: vertex_slot.pos as _,
            base_index: index_slot.pos as _,
            vertex_count: index_slot.len as _,
            base_instance: 0,
            instance_count: 1,
        })
    }
}


pub struct MeshAllocatorStats {
    vertex_saturation: u32,
    index_saturation: u32,
    vertex_fragmentation: u32,
    index_fragmentation: u32,
}

impl std::fmt::Display for MeshAllocatorStats {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        format!(
            "V[{}/{}] I[{}/{}]",
            self.vertex_saturation,
            self.vertex_fragmentation,
            self.index_saturation,
            self.index_fragmentation,
        ).fmt(f)
    }
}


struct MeshData {
    vertices: Allocation,
    indices: Allocation,
}
