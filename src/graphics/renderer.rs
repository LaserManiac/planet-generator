use std::sync::Arc;

use glm::DVec3;
use slotmap::{new_key_type, SlotMap};
use winit::window::Window;

use crate::{
    camera::Camera,
    graphics::{
        bind_group::{BindGroup, BindGroupData},
        block_data::BlockData,
        gpu::Gpu,
        layout_manager::LayoutManager,
        mesh::{
            allocator::{Mesh, MeshAllocator},
            batch::MeshBatch,
            renderer::MeshRenderer,
            VertexData,
        },
        renderer::environment::{Environment, EnvironmentBlock},
        shader::ShaderManager,
        terrain::{
            TerrainBatch,
            TerrainBatchData,
            TerrainInstance,
            TerrainId,
            TerrainVertex,
        },
        uniform_block::UniformBlock,
    },
    loader::Loader,
};

pub mod environment;


new_key_type! {
    pub struct TerrainChunk;
}


pub struct Renderer {
    window_size: [u32; 2],
    gpu: Gpu,
    depth_buffer: wgpu::Texture,
    layout_manager: LayoutManager,
    shader_manager: ShaderManager,
    environment: BindGroup<Environment>,
    terrain_allocator: MeshAllocator<TerrainVertex, u32>,
    terrain_batch: MeshBatch<TerrainBatch, TerrainInstance>,
    terrain_renderer: MeshRenderer<TerrainVertex, Environment, TerrainBatch, TerrainInstance>,
    clear_color: wgpu::Color,
}

impl Renderer {
    pub fn new(window: &Window, loader: &Loader) -> Self {
        let gpu = Gpu::new(window)
            .expect("Could not create GPU!");
        let window_size = window.inner_size();
        let width = window_size.width;
        let height = window_size.height;
        let window_size = [width, height];
        let depth_buffer = create_depth_buffer(&gpu.device, width, height);

        let mut layout_manager = create_layout_manager(Arc::clone(&gpu.device));
        let mut shader_manager = ShaderManager::new(Arc::clone(&gpu.device));
        let environment = BindGroup::new(
            Environment::new(Default::default(), &gpu.device),
            &layout_manager,
            &gpu.device,
        );

        let vertex_capacity = 0x1 << 21;
        let index_capacity = 0x1 << 22;
        let terrain_allocator = MeshAllocator::new(vertex_capacity, index_capacity, &gpu.device);
        let terrain_group = TerrainBatch::new(Default::default(), &gpu.device);
        let terrain_batch = MeshBatch::new(1000, terrain_group, &layout_manager, &gpu.device);
        let terrain_shader = shader_manager.create_or_swap("shader\\terrain.wgsl", loader);
        let terrain_renderer = MeshRenderer::new(
            terrain_shader,
            &shader_manager,
            &layout_manager,
            &gpu.device,
        );

        Self {
            window_size,
            gpu,
            depth_buffer,
            layout_manager,
            shader_manager,
            environment,
            terrain_allocator,
            terrain_batch,
            terrain_renderer,
            clear_color: wgpu::Color::BLACK,
        }
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        self.gpu.resize(width, height);
        self.depth_buffer = create_depth_buffer(&self.gpu.device, width, height);
    }

    pub fn create_terrain(
        &mut self,
        position: DVec3,
        vertices: &[TerrainVertex],
        indices: &[u32],
        instance: TerrainInstance,
    ) -> Result<TerrainId, String> {
        let mesh = self.terrain_allocator.alloc(vertices, indices, &self.gpu.queue)
            .ok_or_else(|| format!("Could not allocate terrain chunk mesh!"))?;
        let instance = self.terrain_batch.create_instance(mesh, instance, &self.terrain_allocator)
            .ok_or_else(|| format!("Could not allocate terrain chunk mesh instance!"))?;
        Ok(TerrainId(instance))
    }
    
    pub fn remove_terrain(&mut self, id: TerrainId) {
        let TerrainId(instance) = id;
        let mesh = self.terrain_batch.destroy_instance(instance);
        self.terrain_allocator.free(mesh);
    }

    pub fn render(&mut self, camera: &Camera) {
        self.gpu.device.poll(wgpu::Maintain::Poll);
        let frame = self.gpu.next_frame();

        let aspect = self.window_size[0] as f32 / self.window_size[1] as f32;
        let view_projection = camera.view_projection(aspect);

        self.environment.block.set(EnvironmentBlock {
            view_projection: view_projection.into(),
            sun_direction: glm::vec3(0.0, 0.0, -1.0).into(),
        });
        self.environment.flush(&self.gpu.queue);

        self.terrain_batch.flush(&self.gpu.queue);
        self.terrain_renderer.manage_state(&self.gpu.device, &self.shader_manager);

        let mut cmd = self.gpu.device.create_command_encoder(&Default::default());
        let depth_view = self.depth_buffer.create_view(&Default::default());
        let mut pass = Self::clear_screen(
            self.clear_color,
            &frame.output.view,
            &depth_view,
            &mut cmd,
        );

        self.terrain_renderer.render(
            &self.environment,
            &self.terrain_batch,
            &self.terrain_allocator,
            &mut pass,
        );

        drop(pass);
        self.gpu.queue.submit(Some(cmd.finish()));
    }

    pub fn clear_color(&self) -> &wgpu::Color {
        &self.clear_color
    }

    pub fn set_clear_color(&mut self, color: wgpu::Color) {
        self.clear_color = color;
    }

    pub fn terrain_wireframe(&self) -> bool {
        self.terrain_renderer.wireframe()
    }

    pub fn set_terrain_wireframe(&mut self, wireframe: bool) {
        self.terrain_renderer.set_wireframe(wireframe);
    }

    fn clear_screen<'a>(
        color: wgpu::Color,
        target: &'a wgpu::TextureView,
        depth: &'a wgpu::TextureView,
        cmd: &'a mut wgpu::CommandEncoder,
    ) -> wgpu::RenderPass<'a> {
        cmd.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[
                wgpu::RenderPassColorAttachment {
                    view: target,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(color),
                        store: true,
                    },
                },
            ],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: &depth,
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(1.0),
                    store: true,
                }),
                stencil_ops: None,
            }),
        })
    }
}


fn create_layout_manager(device: Arc<wgpu::Device>) -> LayoutManager {
    let mut layout_manager = LayoutManager::new(device);
    layout_manager.register_bind_group::<Environment>();
    layout_manager.register_batch_bind_group::<TerrainBatch, TerrainInstance>();
    return layout_manager;
}


fn create_depth_buffer(device: &wgpu::Device, width: u32, height: u32) -> wgpu::Texture {
    device.create_texture(&wgpu::TextureDescriptor {
        label: Some("depth_buffer"),
        size: wgpu::Extent3d {
            width,
            height,
            depth_or_array_layers: 1,
        },
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Depth32Float,
        usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
    })
}
