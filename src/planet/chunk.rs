use glm::DVec3;
use slotmap::new_key_type;

new_key_type! { pub struct ChunkId; }


#[derive(Copy, Clone)]
pub struct ChunkCoords {
    x: u32,
    y: u32,
    depth: u32,
}

impl ChunkCoords {
    pub fn new(x: u32, y: u32, depth: u32) -> Self {
        let size = 2u32.pow(depth);
        assert!(x < size);
        assert!(y < size);
        Self { x, y, depth }
    }

    pub fn x(&self) -> u32 {
        self.x
    }

    pub fn y(&self) -> u32 {
        self.y
    }

    pub fn depth(&self) -> u32 {
        self.depth
    }

    pub fn contains(&self, coord: DVec3) {
        let x = self.x as f64;
        let y = self.x as f64;
        let size = // TODO: How do we calculate chunk dimensions without leaf_size?
        coord.x > self.x
    }
}


pub struct Chunk {
    pub coords: ChunkCoords,
    pub parent: Option<ChunkId>,
    pub children: Option<[ChunkId; 4]>,
}

impl Chunk {
    pub fn new(coords: ChunkCoords, parent: Option<ChunkId>) -> Self {
        Self {
            coords,
            parent,
            children: None,
        }
    }
}
