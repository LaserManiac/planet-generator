use glm::DVec3;

pub type FaceTransformFn<T> = dyn Fn(T, T, T) -> (T, T, T);

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum Face {
    XPlus,
    XMinus,
    YPlus,
    YMinus,
    ZPlus,
    ZMinus,
}

impl Face {
    pub fn from_index(idx: usize) -> Self {
        match idx {
            0 => Self::XPlus,
            1 => Self::XMinus,
            2 => Self::YPlus,
            3 => Self::YMinus,
            4 => Self::ZPlus,
            5 => Self::ZMinus,
            _ => panic!("Invalid face index {}", idx),
        }
    }

    pub fn index(self) -> usize {
        match self {
            Self::XPlus => 0,
            Self::XMinus => 1,
            Self::YPlus => 2,
            Self::YMinus => 3,
            Self::ZPlus => 4,
            Self::ZMinus => 5,
        }
    }

    pub fn opposite(self) -> Self {
        match self {
            Self::XPlus => Self::XMinus,
            Self::XMinus => Self::XPlus,
            Self::YPlus => Self::YMinus,
            Self::YMinus => Self::YPlus,
            Self::ZPlus => Self::ZMinus,
            Self::ZMinus => Self::ZPlus,
        }
    }

    pub fn transform<T>(&self) -> &'static FaceTransformFn<T>
        where T: std::ops::Neg<Output=T> {
        match self {
            Self::XPlus => &|x, y, z| (z, x, y),
            Self::XMinus => &|x, y, z| (-z, -x, y),
            Self::YPlus => &|x, y, z| (-x, z, y),
            Self::YMinus => &|x, y, z| (x, -z, y),
            Self::ZPlus => &|x, y, z| (x, y, z),
            Self::ZMinus => &|x, y, z| (-x, y, -z),
        }
    }

    pub fn inv_transform<T>(&self) -> &'static FaceTransformFn<T>
        where T: std::ops::Neg<Output=T> {
        match self {
            Self::XPlus => &|x, y, z| (y, z, x),
            Self::XMinus => &|x, y, z| (-y, z, -x),
            Self::YPlus => &|x, y, z| (-x, z, y),
            Self::YMinus => &|x, y, z| (x, z, -y),
            Self::ZPlus => &|x, y, z| (x, y, z),
            Self::ZMinus => &|x, y, z| (-x, y, -z),
        }
    }

    pub fn get_local_coord(&self, coord: DVec3, max_depth: u32, radius: f64) -> DVec3 {
        let sphere_coord = coord.normalize() * radius;
        let [x, y, z] = sphere_coord.into();
        let (x, y, z) = self.inv_transform()(x, y, z);
        let factor = radius / z;
        let offset = radius - coord.norm();
        glm::vec3(
            x * factor + radius,
            y * factor + radius,
            z + offset,
        )
    }
}
