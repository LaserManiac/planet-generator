use std::{
    path::{Path, PathBuf},
    sync::atomic::{AtomicUsize, Ordering},
    sync::mpsc::Receiver,
    time::Duration,
};

use notify::{DebouncedEvent, RecommendedWatcher, RecursiveMode, Watcher as _};

pub struct Loader {
    root: PathBuf,
    _watcher: RecommendedWatcher,
    receiver: Receiver<DebouncedEvent>,
    counter: AtomicUsize,
}

impl Loader {
    pub fn new(root: impl AsRef<Path>) -> Self {
        let (sx, rx) = std::sync::mpsc::channel();
        let mut watcher = notify::watcher(sx, Duration::from_millis(1000))
            .expect("Could not create file watcher!");
        let root = root.as_ref().canonicalize()
            .expect("Could not canonicalize loader path!");
        watcher.watch(&root, RecursiveMode::Recursive)
            .expect("Could not start file system watcher!");

        Self {
            root,
            _watcher: watcher,
            receiver: rx,
            counter: AtomicUsize::new(0),
        }
    }

    pub fn load_to_string(&self, path: impl AsRef<Path>) -> Result<String, std::io::Error> {
        let path = self.root.join(path).canonicalize()?;
        let counter = self.counter.fetch_add(1, Ordering::SeqCst);
        println!("[{}] Loading [{}]...", counter, path.display());
        std::fs::read_to_string(path)
    }

    pub fn changed<'a>(&'a self) -> impl Iterator<Item=PathBuf> + 'a {
        std::iter::from_fn(move || {
            match self.receiver.try_recv().ok()? {
                DebouncedEvent::NoticeWrite(path) if path.is_file() =>
                    path.strip_prefix(&self.root).ok().map(ToOwned::to_owned),
                _ => None,
            }
        })
    }
}
