[[block]]
struct Environment {
    view_projection: mat4x4<f32>;
    sun_direction: vec3<f32>;
};

[[block]]
struct Batch {
    camera_position: vec3<f32>;
    time: f32;
};

struct Instance {
    transform: mat4x4<f32>;
};

[[block]]
struct Instances {
    instances: [[stride(64)]] array<Instance>;
};


[[group(0), binding(0)]]
var<uniform> environment: Environment;

[[group(1), binding(0)]]
var<uniform> batch: Batch;

[[group(1), binding(1)]]
var<storage> instance_buffer: [[access(read)]] Instances;


struct Attributes {
    [[builtin(instance_index)]] index: u32;
    [[location(0)]] position: vec3<f32>;
    [[location(1)]] color: vec3<f32>;
};


struct Varying {
    [[builtin(position)]] position: vec4<f32>;
    [[location(0)]] local_position: vec3<f32>;
    [[location(1)]] color: vec3<f32>;
    [[location(2)]] time: f32;
};


struct FragmentOutput {
    [[location(0)]] color: vec4<f32>;
};


fn sample(local: vec3<f32>) -> vec3<f32> {
    return vec3<f32>(0.0, 1.0, 0.0);
}


[[stage(vertex)]]
fn vertex_main(a: Attributes) -> Varying {
    var view_projection: mat4x4<f32> = environment.view_projection;
    var instance: Instance = instance_buffer.instances[a.index];
    var model: mat4x4<f32> = instance.transform;
    var position: vec4<f32> = vec4<f32>(a.position, 1.0);
    var world_pos: vec4<f32> = model * position;

    var o: Varying;
    o.position = view_projection * world_pos;
    o.local_position = a.position;
    o.color = a.color;
    o.time = batch.time;
    return o;
}

[[stage(fragment)]]
fn fragment_main(i: Varying) -> FragmentOutput {
    var final: vec3<f32> = sample(vec3<f32>(0.0, 0.0, 0.0));

    var o:  FragmentOutput;
    o.color = vec4<f32>(final, 1.0);
    return o;
}
